import os, urllib, json , pika, ConfigParser, syslog
from readConfig import ReadConfig as conf


def getDevices():
    cfg = conf()
    url = cfg.osase_api_url() + "/devices"
    try :
        response = urllib.urlopen(url)
        data = json.loads(response.read())
        #print data
        return data
    except urllib.error.URLError as e:
        syslog.syslog(syslog.LOG_ERR, 'ERROR Unable to get devices  : ' + e.reason)


def pushIPtoMq(ip,device_id,status):

    cfg = conf()
    #Create a new instance of the Connection object
    
    credentials = pika.PlainCredentials(cfg.activemq_user(), cfg.activemq_password())
    parameters = pika.ConnectionParameters(cfg.activemq_server(),
                                       5672,
                                       '/',
                                       credentials)

    #parameters = pika.URLParameters('amqp://' + cfg.activemq_user() +':'+ cfg.activemq_password() +'@' + cfg.activemq_server() +':5672/%2F')
   
    connection = pika.BlockingConnection(parameters)

    #Create a new channel with the next available channel number or pass in a channel number to use
    channel = connection.channel()
     #define exchange
    #channel.exchange_declare(exchange=activemq_exchange_name, exchange_type='fanout')
  
    #Declare queue, create if needed. This method creates or checks a queue. When creating a new queue the client can specify various properties that control the durability of the queue and its contents, and the level of sharing for the queue.
    jsonstr = {"ipaddress":ip,"devices_id":device_id,"status":status}
    jsondata = json.dumps(jsonstr)
    channel.queue_declare(queue=cfg.ip_queue_name(),durable=True)
    channel.basic_publish(exchange='', routing_key=cfg.ip_queue_name(), body=jsondata,
                      properties=pika.BasicProperties(
                         delivery_mode = 2, # make message persistent
                      ))    
    #print ("[x] Sent " + ip)
    syslog.syslog(syslog.LOG_ERR, "[x] Sent " + ip)
    connection.close()
#Main Thread
if __name__ == '__main__':
    ipadd = getDevices()
    for i in ipadd:
        pushIPtoMq(i['ipaddress'],i['device_id'],i['status'])