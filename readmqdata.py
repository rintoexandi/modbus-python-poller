import os, sys, urllib, json , pika, ConfigParser
import time
import socket
import Queue
import threading
from datetime import datetime 
import pytz
from readConfig import ReadConfig as conf
from pyModbusTCP.client import ModbusClient
import syslog

#Set Total Register need to take care of
max_register=100 #Change this
max_bits=64
socket_port=502
socket_timeout=3
server_is_online=False


#Threading
poll_queue = Queue.Queue()
print_queue = Queue.Queue()
max_number_thread=5

def getData():
    
        cfg = conf()

        credentials = pika.PlainCredentials(cfg.activemq_user(), cfg.activemq_password())
        parameters = pika.ConnectionParameters(cfg.activemq_server(),
                                        5672,
                                        '/',
                                        credentials)

        #parameters = pika.URLParameters('amqp://' + cfg.activemq_user() +':'+ cfg.activemq_password() +'@' + cfg.activemq_server() +':5672/%2F')
    
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        channel.queue_declare(queue=cfg.ip_queue_name(),durable=True)
        channel.basic_consume(queue=cfg.ip_queue_name(),on_message_callback=callback)
        print (' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()

        #poll_queue.task_done()

def callback(ch, method, properties, body):

        jsondata = json.loads(body)
        print("JSON Data" + str(jsondata))

        ipaddress = jsondata.get('ipaddress')
        devices_id= jsondata.get('devices_id')
        device_prev_status = jsondata.get('status')
        #Generate Json
        generatejson(ipaddress,devices_id,device_prev_status)
        ch.basic_ack(delivery_tag = method.delivery_tag)
    
def isAlive(host) :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(socket_timeout)
    result = sock.connect_ex((str(host),socket_port))
    if result == 0:
       return True
    else:
       return False




def get_holding_register(host):
    #calculatedigital(device_id)

    print ("Retrieving modbus data from %r" % host) 
    
    if (isAlive(host)==True) :   
        c = ModbusClient()
        # define modbus server host, port
        c.host(host)
        c.port(socket_port)

        try:

            c.open()
        
            if c.is_open():
                  # read registers at address 0, store result in regs list
                regs = c.read_holding_registers(0, max_register)
                
                if regs:
                    #data_reg = json.dumps(regs)
                    return regs
                
        except (KeyboardInterrupt, SystemExit):
                raise
        except:
                pass
    else:
        errmsg = "Host "+host+":"+str(socket_port) + " maybe going somewhere, cannot contacted"
        syslog.syslog(syslog.LOG_ERR, errmsg)



#Main task, get modbus data from RTU
def get_modbus_data(host,analog) :
    global server_is_online 
    #print ("Retrieving modbus data analog=" + str(analog) +" from %r" % host) 
    modbusdata={}
    if (isAlive(host)==True) :  
        server_is_online=True 
        c = ModbusClient()
        # define modbus server host, port
        c.host(host)
        c.port(socket_port)

        #Set Default values

        try:
            c.open()
        
            if c.is_open():

                if analog :
                    # read registers at address 0, store result in regs list
                    modbusdata = c.read_holding_registers(0, max_register)

                    
                if not analog:
                    # read coils at address 0, store result in regs list
                    modbusdata = c.read_coils(0,max_bits)
                
                if modbusdata:
                   #print('Modbuse Data' + str(modbusdata))
                   return modbusdata
                
                if not modbusdata:
                    return None
            else:

                if analog:
                    modbusdata = zerovalues()
                
                if not analog:
                    modbusdata = falsevalues()

                return modbusdata

        except (KeyboardInterrupt, SystemExit):
                raise
        except:
                pass
    else:
        if analog:
            modbusdata = zerovalues()
                
        if not analog:
            modbusdata = falsevalues()
        errmsg = "Host "+host+":"+str(socket_port) + " maybe going somewhere, cannot contacted"
        syslog.syslog(syslog.LOG_ERR, errmsg)
        server_is_online=False
        return modbusdata

#Get list of deviceports configuration from api
#Params device_id : int
#Return json list of deviceport
def getdeviceport(device_id,isAnalog):

    cfg = conf()
    url = cfg.osase_api_url() + "/devices/deviceport-analog?id="+str(device_id)

    if isAnalog:
        url = cfg.osase_api_url() + "/devices/deviceport-analog?id="+str(device_id)
    if not isAnalog:
        url = cfg.osase_api_url() + "/devices/deviceport-digital?id="+str(device_id)

    try :
        #print(url)
        response = urllib.urlopen(url)
        data = response.read()
        #print data
        jsondata = json.loads(data)
        #print jsondata
        return jsondata
    except urllib.error.URLError as e:
        print(e.reason)  

def zerovalues():
    listofzeros = [0] * max_register
    return listofzeros

def falsevalues():
    listoffalse = [True] * max_bits
    return listoffalse

#Parse data, compare deviceport_id from api and data from holdingregister
#params deviceports : deviceportlist from api
def parse_holding_register(deviceports,holdingregister):   
    #dev = getdeviceport(1)
    #print(dev)
    #register = get_holding_register("192.168.56.101",1)
    #print('Holding Register' + str(holdingregister))

    deviceresult = []
    #print('Register -->' + str(register))
    for i in deviceports:
        #print('Device Port ID : ' + str(i['deviceport_id']))

        tmp = {}
        #tmp['device_id']=1
        #tmp['processingtime'] = processingtime
        #deviceresult.append(tmp)
        
        #portregister = []
        reg_port = ''
        por_regs = i['portregister']
        scalefactor = i['scalefactor']
        #print(str(por_regs))
        binreg=''
        deviceportvalues = 0
        for x in por_regs:
            
            
            #print('Port Register ID : ' + str(x['registeredports']))
            #bin2 = bin(register[1])
            #bin3 = bin(regs[2])

            #print('Data From Register : ' +str(register[x['registeredports']]) )
            binarytmp = bin(holdingregister[x['registeredports']])
            reg_port = reg_port  + str(x['registeredports']) + ','
            str_bin_2 = str(binarytmp)
            binreg = binreg + str_bin_2[2:]
            str_part_bin2=binreg
              #int_join = int(str_join)
            dec = binaryToDecimal(int(str_part_bin2))
            #print ('Binary : ' + str_part_bin2 + ' Decimal : ' + str(dec))

            deviceportvalues = dec * scalefactor
            
        '''
        Device Port ID : 82
        Port Register ID : 33
        Port Register ID : 34
        Device Port ID : 83
        Port Register ID : 35
        Port Register ID : 36
        Device Port ID : 84
        Port Register ID : 37
        Port Register ID : 38
        Device Port ID : 85
        Port Register ID : 39
        Port Register ID : 40
        '''
        #tmpport = {}
        tmp["isdigital"]=i['isdigital']
        tmp["prevval"]=i["value"]
        tmp["registeredports"]=str(reg_port)
        tmp["value"] = deviceportvalues
        tmp["deviceport_id"]=i['deviceport_id']
       
        deviceresult.append(tmp)

    json_data = deviceresult

    #print('JSON Result ' + str(json_data))

    return json_data

    #Parse data, compare deviceport_id from api and data from holdingregister
#params deviceports : deviceportlist from api
def parse_coil_values(deviceports,coilvalues):   
    #dev = getdeviceport(1)
    #print(dev)
    #register = get_holding_register("192.168.56.101",1)
    #print('Coil Values-----> ' + str(coilvalues))
    #print('Device Ports -----> ' + str(deviceports))
    deviceresult = []
    #print('Register -->' + str(register))
    for i in deviceports:
        #print('Device Port ID : ' + str(i['deviceport_id']))

        tmp = {}
        #tmp['device_id']=1
        #tmp['processingtime'] = processingtime
        #deviceresult.append(tmp)
        
        #portregister = []
        
        por_regs = i['portregister']
        #print(str(por_regs))
        binreg=''
        coil_values = 0
        for x in por_regs:
            
            
            #print('Port Register ID : ' + str(x['registeredports']))
            #bin2 = bin(register[1])
            #bin3 = bin(regs[2])
            #print('Data From Register : ' +str(register[x['registeredports']]) )
            coil_values = convert_coil_values(coilvalues[x['registeredports']])
        tmp["isdigital"]=i['isdigital']
        tmp["prevval"]=i["value"]
        tmp["registeredports"]=x['registeredports']
        tmp["value"] = coil_values
        tmp["deviceport_id"]=i['deviceport_id']
       
        deviceresult.append(tmp)

    json_data = deviceresult

    #print('JSON Result ' + str(json_data))

    return json_data
def convert_coil_values(strcoilvalues):
    if (str(strcoilvalues).upper()=="FALSE"):
        return 1
    else:
        return 0

def generatejson(host,device_id,prev_status):
    #get deviceports
    analogdeviceports = getdeviceport(device_id,True)

    #get digitaldeviceports
    digitaldeviceports = getdeviceport(device_id,False)

    #getholdingregister
    holdingregister = get_modbus_data(host,True)

    #get digitalportvalues
    coilvalues =  get_modbus_data(host,False)

    #Process Data
    json_reg_data = parse_holding_register(analogdeviceports,holdingregister)
    
    json_bin_data = parse_coil_values(digitaldeviceports,coilvalues)

    
    tz = pytz.timezone('Asia/Jakarta')   
    processingtime = datetime.now(tz).strftime('%Y:%m:%d %H:%M:%SC')       
    
    
    jsonfinal = {'prev_status':prev_status,'isonline':server_is_online,'processingtime' : str(processingtime) , 'address':host, 'device_id': str(device_id), 'analogports':json_reg_data,'digitalports':json_bin_data}

    json_data = json.dumps(jsonfinal)
    
    #print(str(json_data))

    sendtoqueue(json_data)

def sendtoqueue(data):

    cfg = conf()
    #Create a new instance of the Connection object
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host=cfg.activemq_server()))
    credentials = pika.PlainCredentials(cfg.activemq_user(), cfg.activemq_password())
    parameters = pika.ConnectionParameters(cfg.activemq_server(),
                                       5672,
                                       '/',
                                       credentials)

    #parameters = pika.URLParameters('amqp://' + cfg.activemq_user() +':'+ cfg.activemq_password() +'@' + cfg.activemq_server() +':5672/%2F')
   
    connection = pika.BlockingConnection(parameters)
    #Create a new channel with the next available channel number or pass in a channel number to use
    channel = connection.channel()
     #define exchange
    #channel.exchange_declare(exchange=activemq_exchange_name, exchange_type='fanout')
  
    #Declare queue, create if needed. This method creates or checks a queue. When creating a new queue the client can specify various properties that control the durability of the queue and its contents, and the level of sharing for the queue.
    channel.queue_declare(queue=cfg.poller_queue_name(),durable=True)
    channel.basic_publish(exchange='', routing_key=cfg.poller_queue_name(), body=data,
                      properties=pika.BasicProperties(
                         delivery_mode = 2, # make message persistent
                      ))    
    #print ("[**] data modbus sent to queue")
    errmsg = "[**] data modbus sent to queue"
    syslog.syslog(syslog.LOG_INFO, errmsg)
    connection.close()

#Convert Binary format to Decimal
def binaryToDecimal(binary): 
      
    binary1 = binary 
    decimal, i, n = 0, 0, 0
    while(binary != 0): 
        dec = binary % 10
        decimal = decimal + dec * pow(2, i) 
        binary = binary//10
        i += 1
    
    return decimal
#Main Thread
if __name__ == '__main__':

   
    getData()
     