#!/bin/bash
PIDFILES=/var/osase-poller/pid/*.pid
for f in $PIDFILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  cat $f | xargs kill -9
  echo "Removing file..." . $f
  rm -fr $f
done
