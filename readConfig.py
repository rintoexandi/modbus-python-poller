import os, sys, ConfigParser

class ReadConfig:

    def __init__(self):
        install_dir = os.path.dirname(os.path.realpath(__file__))
        config_file = install_dir + '/poller.cfg'
        try:
            with open(config_file) as f:
                pass
        except IOError as e:
            print "ERROR: Oh dear... %s does not seem readable" % config_file
            sys.exit(2)
        try:
            config = ConfigParser.ConfigParser()
            config.read(config_file)
            #Set  param
            self._activemq_server = config.get("DEFAULT","activemq_server")
            self._activemq_user = config.get("DEFAULT","activemq_user")
            self._activemq_password = config.get("DEFAULT","activemq_password")
            self._activemq_exchange_name = config.get("DEFAULT","activemq_exchange_name")
            self._ip_queue_name = config.get("DEFAULT","ip_queue_name")
            self._poller_queue_name = config.get("DEFAULT","poller_queue_name")
            self._osase_api_url=config.get("DEFAULT","osase_api_url")
            
            
        except ConfigParser.NoOptionError:
            print "ERROR: Options config is not available"
            sys.exit(2)
       
    
    def activemq_server(self):
        return self._activemq_server
    
    def activemq_user(self):
        return self._activemq_user

    def activemq_password(self):
        return self._activemq_password
    
    def activemq_exchange_name(self):
        return self._activemq_exchange_name

    def ip_queue_name(self):
        return self._ip_queue_name
    
    def poller_queue_name(self):
        return self._poller_queue_name
    
    def osase_api_url(self):
        return self._osase_api_url

    